var map;
var marker;

jQuery(function($) {'use strict',

	//#main-slider
	$(function(){
		$('#main-slider.carousel').carousel({
			interval: 8000
		});
	});

	$(function(){
		$('.carousel-inner').carousel({
			interval: 3000
		})
	});


	$(function () {
	    $(window).scroll(sticky_relocate);
	    sticky_relocate();
	});

	// accordian
	$('.accordion-toggle').on('click', function(){
		$(this).closest('.panel-group').children().each(function(){
		$(this).find('>.panel-heading').removeClass('active');
		 });

	 	$(this).closest('.panel-heading').toggleClass('active');
	});

	//Initiat WOW JS
	new WOW().init();

	// portfolio filter
	$(window).load(function(){'use strict';
		var $portfolio_selectors = $('.portfolio-filter >li>a');
		var $portfolio = $('.portfolio-items');
		$portfolio.isotope({
			itemSelector : '.portfolio-item',
			layoutMode : 'fitRows'
		});
		
		$portfolio_selectors.on('click', function(){
			$portfolio_selectors.removeClass('active');
			$(this).addClass('active');
			var selector = $(this).attr('data-filter');
			$portfolio.isotope({ filter: selector });
			return false;
		});
	});

	// Contact form
	var form = $('#main-contact-form');
	form.submit(function(event){
		event.preventDefault();
		var form_status = $('<div class="form_status"></div>');
		$.ajax({
			url: $(this).attr('action'),

			beforeSend: function(){
				form.prepend( form_status.html('<p><i class="fa fa-spinner fa-spin"></i> Email is sending...</p>').fadeIn() );
			},
			success: function(data) {
				console.log(data);
			}
		}).done(function(data){
			form_status.html('<p class="text-success">' + data.message + '</p>').delay(3000).fadeOut();
		});
	});

	//goto top


	$(window).scroll(function() {
		var window_top = $(window).scrollTop();

		if (window_top < 200) {
			$('.gototop').hide('slow');
		}else {
			$('.gototop').show('slow');
		}

	})

	$('.gototop').click(function(event) {
		event.preventDefault();
		$('html, body').animate({
			scrollTop: $("body").offset().top
		}, 500);
	});	

	

	//Pretty Photo
	$("a[rel^='prettyPhoto']").prettyPhoto({
		social_tools: false
	});	
});

function initMap() {
  map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: -6.292748, lng: 106.797743},
    zoom: 15,
    zoomControl: false,
    scaleControl: false,
    scrollwheel: false,
    mapTypeControl: false
  });

marker = new google.maps.Marker({
    map: map,
    draggable: true,
    animation: google.maps.Animation.DROP,
    position: {lat: -6.292748, lng: 106.797743}
    });
      marker.addListener('click', toggleBounce);
}

function sticky_relocate() {
    var div_top = 130;
    var window_top = $(window).scrollTop();

    // $('#sticky-anchor').offset().top;
    if (window_top > div_top) {
        $('#main-header').addClass('stick');
    } else {
        $('#main-header').removeClass('stick');
    }
}
